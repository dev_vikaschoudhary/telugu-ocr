# -*- coding: utf-8 -*-
"""
Created on Thu Jan  4 11:51:12 2018

@author: vikas
"""

import tensorflow as tf
import numpy as np


def randomize(data, labels):
    perm = np.random.permutation(labels.shape[0])
    random_data = data[perm, :, :]
    random_labels = labels[perm]
    return random_data, random_labels

def nextBatch(data, labels, start_index):
    end_index = int(data.shape[0]/batch_size) + start_index
    return data[start_index: end_index, :, :, :], labels[start_index: end_index], end_index

learning_rate = 0.005
training_epochs = 60
batch_size = 5
dropout = 0.75
previous_loss = -1

n_input = 1024  # MNIST data input (img shape: 28*28)
n_classes = 20  # MNIST total classes (0-9 digits)
n_hidden_layer = 3072 # layer number of features & width of the network
#n_hidden_layer2 = 200 # layer number of features & width of the network

training = np.load('data0_19_n_res32.pickle')
labels = np.load('lables0_19_n_res32.pickle')
labels = labels[~np.all(labels == 0, axis=1)]

count = labels.shape[0]
test_samples = int(count*.85)
validation_samples = int(count*.95)

training_data = training[:test_samples, :, :]
training_labels = labels[:test_samples]

validation_data = training[test_samples:validation_samples, :, :]
validation_labels = labels[test_samples:validation_samples]

test_data = training[validation_samples:count, :, :]
test_labels = labels[validation_samples:count]

training_data, training_labels = randomize(training_data, training_labels)
#validation_data, validation_labels = randomize(validation_data, validation_labels)
#test_data, test_labels = randomize(test_data, test_labels)

weights = {
        'hidden_layer':tf.Variable(tf.random_normal([n_input, n_hidden_layer])),
#        'hidden_layer2':tf.Variable(tf.random_normal([n_hidden_layer, n_hidden_layer2])),
        'out': tf.Variable(tf.random_normal([n_hidden_layer, n_classes]))
        }

biase  = {
        'hidden_layer':tf.Variable(tf.random_normal([n_hidden_layer])),
#        'hidden_layer2':tf.Variable(tf.random_normal([n_hidden_layer2])),
        'out': tf.Variable(tf.random_normal([n_classes]))
        }

# tf Graph input
x = tf.placeholder("float", [None, 32, 32, 1])
y = tf.placeholder("float", [None, n_classes])
keep_prob = tf.placeholder(tf.float32)

x_flat = tf.reshape(x, [-1, n_input])

# Hidden layer with RELU activation
layer_1 = tf.add(tf.matmul(x_flat, weights['hidden_layer']), biase['hidden_layer'])
layer_1 = tf.nn.relu(layer_1)
layer_1 = tf.nn.dropout(layer_1, keep_prob)

#layer_2 = tf.add(tf.matmul(layer_1, weights['hidden_layer2']), biase['hidden_layer2'])
#layer_2 = tf.nn.relu(layer_2)
#layer_2 = tf.nn.dropout(layer_2, keep_prob)

# output layer
logits = tf.add(tf.matmul(layer_1, weights['out']), biase['out'])
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels = y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Accuracy
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

#save_file = './train_model.ckpt'
#saver = tf.train.Saver()

# Initializing the variables
init = tf.global_variables_initializer()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    # Training cycle
    for epoch in range(training_epochs):
       
        index = 0
        for batch in range(batch_size):
            
            batch_x, batch_y, index = nextBatch(training_data, training_labels, index)
            # Run optimization op (backprop) and cost op (to get loss value)
            sess.run(optimizer, feed_dict={x:  batch_x, y: batch_y, keep_prob: .75})
            loss = sess.run(cost, feed_dict={ x: batch_x, y: batch_y, keep_prob: .5})
            valid_acc = sess.run(accuracy, feed_dict={ x: validation_data, y: validation_labels, keep_prob: 1.})
            
            if previous_loss>0 and previous_loss - loss < 0:
                learning_rate = learning_rate*.95
                print ("decreasing learning_rate ",  learning_rate)

            previous_loss = loss

            print('Epoch {:>2} '
                  'Loss: {:>10.4f} Validation Accuracy: {:.6f}'.format(
                epoch + 1,
                loss,
                valid_acc))

    test_acc = sess.run(accuracy, feed_dict={x: test_data, y: test_labels, keep_prob: 1.})
    print('Testing Accuracy: {}'.format(test_acc))
#    saver.save(sess, save_file)
#    print('Trained Model Saved.')