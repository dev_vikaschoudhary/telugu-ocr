# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 15:15:22 2017

@author: vikas
"""

import matplotlib.pyplot as plt
from PIL import Image
import os
from sklearn import svm, metrics
import numpy as np

start = False
x_list = []
y_list = []      

penString = "pen_dataset/"
batch_size = 10000
test_size = 200
n_input = 784  # MNIST data input (img shape: 48*48)
n_classes = 166  # MNIST total classes (0-9 digits)
count = 0
test_count = 0
labels_list = []

training = np.empty(shape = (batch_size, 28, 28), dtype=np.float)
labels = np.zeros(shape = (batch_size, n_classes), dtype=np.float)

for directory in os.listdir(penString):
    for user_dir in os.listdir(penString + directory):
        filename = penString + directory + "/" + user_dir
        
        if ".jpg" in filename:
            continue

        print("Reading " + filename)
        
        with open(filename, encoding="utf-8") as file:
            x = [l.strip() for l in file]
            label = ''
            for word in x:     
            
                if ".SEGMENT CHARACTER" in word:
                    z = word.split() 
                    label = z[4]
            
                if word==".PEN_UP":
                    start = False    
                
                if start:
                   z = word.split() 
                   x_list.append(int(z[0]))
                   y_list.append(int(z[1]))
                 
                if word==".PEN_DOWN":
                    start = True
                    
            if count>batch_size:
                break

            plt.plot(x_list, y_list, 'ro')
            plt.axis('off')
            label = label.replace('"', "")
            img_file = penString + directory + "/" + label +'_.jpg'
                    
            if os.path.exists(img_file):
                img_file = penString + directory + "/" + label +'_1.jpg'
            
            if len(label)==0:
                continue
            
            labels_list.append(int(label))
            print (img_file)
            plt.savefig(img_file)
            jpgfile = Image.open(img_file)
            jpgfile = jpgfile.resize((28, 28), Image.ANTIALIAS)
            gray = jpgfile.convert('L')
            arr = np.array(gray)
            arr = arr.astype(float)
            arr = arr*1/arr.max()
            np.place(arr, arr==1, [0])
            
            training[count] = arr        
            labels[count][int(label)] = 1
            
            count = count +1
            
            plt.clf()
            
            start = False
            x_list = []
            y_list = [] 
            print ("processed " + filename + "\n")

n_samples = int(count*.9)
#
#training_data = training[:n_samples, :, :].reshape((-1, 28*28))
#training_labels = labels_list[:n_samples]
#
#test_data = training[n_samples:count, :, :].reshape((-1, 28*28))
#
#classifier = svm.SVC(gamma=0.001)
#classifier.fit(training_data, training_labels)
#predicted = classifier.predict(test_data)
#expected =  labels_list[n_samples:count]
#print("Classification report for classifier %s:\n%s\n"
#      % (classifier, metrics.classification_report(expected, predicted)))

import tensorflow as tf

# Parameters
learning_rate = 0.001
training_epochs = 200
display_step = 1


n_hidden_layer = 256 # layer number of features & width of the network

weights = {
        'hidden_layer':tf.Variable(tf.random_normal([n_input, n_hidden_layer])),
        'out': tf.Variable(tf.random_normal([n_hidden_layer, n_classes]))
        }

biase  = {
        'hidden_layer':tf.Variable(tf.random_normal([n_hidden_layer])),
        'out': tf.Variable(tf.random_normal([n_classes]))
        }

# tf Graph input
x = tf.placeholder("float", [None, 28, 28])
y = tf.placeholder("float", [None, n_classes])
keep_prob = tf.placeholder(tf.float32)

x_flat = tf.reshape(x, [-1, n_input])

# Hidden layer with RELU activation
layer_1= tf.add(tf.matmul(x_flat, weights['hidden_layer']), biase['hidden_layer'])
layer_1 = tf.nn.relu(layer_1)

# output layer
logits = tf.add(tf.matmul(layer_1, weights['out']), biase['out'])
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels = y))
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(cost)

# Accuracy
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    # Training cycle
    for epoch in range(training_epochs):
        # Run optimization op (backprop) and cost op (to get loss value)
        sess.run(optimizer, feed_dict={x:  training[0:n_samples,:,:], y: labels[0:n_samples,:]})
        loss = sess.run(cost, feed_dict={ x: training[0:n_samples,:,:], y: labels[0:n_samples,:], keep_prob: .5})
        valid_acc = sess.run(accuracy, feed_dict={ x: training[8000:9000,:,:], y: labels[8000:9000,:], keep_prob: 1.})
        
        print('Epoch {:>2} '
                  'Loss: {:>10.4f} Validation Accuracy: {:.6f}'.format(
                epoch + 1,
                loss,
                valid_acc))
