# -*- coding: utf-8 -*-
"""
Created on Thu Dec 14 15:04:01 2017

@author: vikas
"""

import os
from PIL import Image
import numpy as np

batch_size = 200
test_size = 200
n_input = 784  # MNIST data input (img shape: 48*48)
n_classes = 166  # MNIST total classes (0-9 digits)
image_dim = 28
penString = "test_pen/"

training = np.empty(shape = (batch_size, image_dim, image_dim), dtype=np.float)
labels = np.zeros(shape = (batch_size, n_classes), dtype=np.float)

test = np.empty(shape = (test_size, image_dim, image_dim), dtype=np.float)
test_labels = np.zeros(shape = (test_size, n_classes), dtype=np.float)

count = 0
test_count = 0

for directory in os.listdir(penString):
    for user_dir in os.listdir(penString+directory):
        filename = penString + directory + "/" + user_dir
        
        if ".jpg" not in filename:
            continue

        print("Reading " + filename)
        label = filename.split('/')[2].split('_')[0]
        jpgfile = Image.open(filename)

        if count>=batch_size:
            break
        
        if(jpgfile.size[0] == 28):
        
            training[count] = jpgfile        
            labels[count][int(label)] = 1
    
            count = count +1
