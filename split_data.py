# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 08:44:28 2018

@author: vikas
"""

import numpy as np

training = np.load('data0_165_noise_res32.pickle')
labels = np.load('lables0_165_noise_res32.pickle')
count = labels.shape[0]
#labels = labels[~np.all(labels == 0, axis=1)]
max_label = 40
min_label = 20
n_classes = 166 # total classes (0-165 characters)
labels_list = []

new_labels = labels[:, min_label:max_label]
new_labels = new_labels[~np.all(new_labels == 0, axis=1)] 
new_count = new_labels.shape[0]

split_training = np.empty(shape = (new_count, 32, 32, 1), dtype=np.float)
index = 0

try:
    for i in range(count):
        for j in range(min_label, max_label):
            if labels[i, j] == 1:
                split_training[index, :, :, 0] = training[i, :, :, 0] 
                labels_list.append(int(j))
                index = index + 1
except:
    print( "This is an error message!")

split_training.dump('data20_39_n_res32.pickle')
new_labels.dump('lables20_39_n_res32.pickle')
np.asarray(labels_list).dump('lables_list20_39_n_res32.pickle')
