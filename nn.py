# -*- coding: utf-8 -*-
"""
Created on Wed Dec 20 11:51:01 2017

@author: vikas
"""

import numpy as np
import tensorflow as tf

training = np.load('data10_19.pickle')
labels = np.load('lables0_9.pickle')

n_input = 784  # MNIST data input (img shape: 48*48)
n_classes = 10  # MNIST total classes (0-9 digits)
count = labels.shape[0]
training = training[:count,:, :]
n_samples = int(count*.9)

training_data = training[:n_samples, :, :]
training_labels = labels[:n_samples]

test_data = training[n_samples:count, :, :]
test_labels = labels[n_samples:count]


# Parameters
learning_rate = 0.001
training_epochs = 3000
display_step = 1


n_hidden_layer = 256 # layer number of features & width of the network

weights = {
        'hidden_layer':tf.Variable(tf.random_normal([n_input, n_hidden_layer])),
        'out': tf.Variable(tf.random_normal([n_hidden_layer, n_classes]))
        }

biase  = {
        'hidden_layer':tf.Variable(tf.random_normal([n_hidden_layer])),
        'out': tf.Variable(tf.random_normal([n_classes]))
        }

# tf Graph input
x = tf.placeholder("float", [None, 28, 28])
y = tf.placeholder("float", [None, n_classes])
keep_prob = tf.placeholder(tf.float32)

x_flat = tf.reshape(x, [-1, n_input])

# Hidden layer with RELU activation
layer_1= tf.add(tf.matmul(x_flat, weights['hidden_layer']), biase['hidden_layer'])
layer_1 = tf.nn.relu(layer_1)

# output layer
logits = tf.add(tf.matmul(layer_1, weights['out']), biase['out'])
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels = y))
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(cost)

# Accuracy
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    # Training cycle
    for epoch in range(training_epochs):
        # Run optimization op (backprop) and cost op (to get loss value)
        sess.run(optimizer, feed_dict={x:  training_data, y: training_labels})
        loss = sess.run(cost, feed_dict={ x: training_data, y: training_labels, keep_prob: .5})
        valid_acc = sess.run(accuracy, feed_dict={ x: test_data, y: test_labels, keep_prob: 1.})
        
        print('Epoch {:>2} '
                  'Loss: {:>10.4f} Validation Accuracy: {:.6f}'.format(
                epoch + 1,
                loss,
                valid_acc))
