# -*- coding: utf-8 -*-
"""
Created on Wed Dec 20 10:50:01 2017

@author: vikas
"""

import numpy as np
from sklearn import svm, metrics

training = np.load('data20_39_n_res32.pickle')
labels = np.load('lables_list20_39_n_res32.pickle')
#labels = labels[~np.all(labels == 0, axis=1)]

count = labels.shape[0]
training = training[:count,:, :, 0]
n_samples = int(count*.9)

training_data = training[:n_samples, :, :].reshape((-1, 32*32))
training_labels = labels[:n_samples]

test_data = training[n_samples:count, :, :].reshape((-1, 32*32))

classifier = svm.SVC(gamma=0.001)
classifier.fit(training_data, training_labels)
predicted = classifier.predict(test_data)
expected =  labels[n_samples:count]
print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(expected, predicted)))
