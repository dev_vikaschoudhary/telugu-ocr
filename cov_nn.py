# -*- coding: utf-8 -*-
"""
Created on Mon Dec 25 17:10:32 2017

@author: vikas
"""

import numpy as np
import tensorflow as tf

training = np.load('C:/ML/capstone project/data0_19_n_res32.pickle')
labels = np.load('C:/ML/capstone project/lables0_19_n_res32.pickle')
labels = labels[~np.all(labels == 0, axis=1)]

count = labels.shape[0]
n_classes = 20 # total classes (0-165 characters)

batch_size = 5 

def randomize(data, labels):
    perm = np.random.permutation(labels.shape[0])
    random_data = data[perm, :, :]
    random_labels = labels[perm]
    return random_data, random_labels

def nextBatch(data, labels, start_index):
    end_index = int(data.shape[0]/batch_size) + start_index
    return data[start_index: end_index, :, :, :], labels[start_index: end_index], end_index

def lrelu(x, alpha):
  return tf.nn.relu(x) - alpha * tf.nn.relu(-x)

training = training[:count,:, :, :]
test_samples = int(count*.85)
validation_samples = int(count*.95)

training_data = training[:test_samples, :, :]
training_labels = labels[:test_samples]

validation_data = training[test_samples:validation_samples, :, :]
validation_labels = labels[test_samples:validation_samples]

test_data = training[validation_samples:count, :, :]
test_labels = labels[validation_samples:count]

training_data, training_labels = randomize(training_data, training_labels)
validation_data, validation_labels = randomize(validation_data, validation_labels)
#test_data, test_labels = randomize(test_data, test_labels)

#learning_rate = 0.009
#global_step = tf.Variable(0, trainable=False)
learning_rate = 0.01
previous_loss = -1

plot_data = np.empty(shape = (100, 3), dtype=np.float)

#k = 0.5
#learning_rate = tf.train.inverse_time_decay(learning_rate, global_step, k)

#global_step = tf.Variable(0)
#learning_rate = tf.train.exponential_decay(3.0, global_step, 3, 0.3, staircase=True)

epochs = 100

dropout = 0.75

weights = {
           'wc1': tf.Variable(tf.random_normal([3, 3, 1, 32])), 
           'wc2': tf.Variable(tf.random_normal([3, 3, 32, 64])),
           'wd1': tf.Variable(tf.random_normal([8*8*64, 1536])),
           'out': tf.Variable(tf.random_normal([1536, n_classes]))
          }

biases = {  
            'bc1': tf.Variable(tf.random_normal([32])),  
            'bc2': tf.Variable(tf.random_normal([64])) ,   
            'bd1': tf.Variable(tf.random_normal([1536])),
            'out': tf.Variable(tf.random_normal([n_classes]))    
        }

def conv2d(x, W, b, strides = 1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
#    return lrelu(x, alpha)
    return tf.nn.relu(x)
#    return tf.nn.leaky_relu(x, alpha)
    
def maxpool2d(inputValues, k=2):
    return tf.nn.max_pool(inputValues, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')
#    return tf.nn.avg_pool(inputValues, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')

def conv_net(x, weights, biases, dropout):
    
    #Layer 1
    conv1 = conv2d(x, weights['wc1'], biases['bc1'])
    conv1 = maxpool2d(conv1, k=2)
    
    #Layer 2
    conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
    conv2 = maxpool2d(conv2, k=2)

#    conv3 = conv2d(conv2, weights['wc3'], biases['bc3'])
#    conv3 = maxpool2d(conv3, k=2)
    
    #connected layer
    fc1 = tf.reshape(conv2, [-1, weights['wd1'].get_shape().as_list()[0]])
    fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
    fc1 = tf.nn.relu(fc1)
    fc1 = tf.nn.dropout(fc1, dropout)
    
#    fc2 = tf.reshape(fc1, [-1, weights['wd2'].get_shape().as_list()[0]])
#    fc2 = tf.add(tf.matmul(fc2, weights['wd2']), biases['bd2'])
#    fc2 = tf.nn.relu(fc2)
#    fc2 = tf.nn.dropout(fc2, dropout)
    
    output = tf.add(tf.matmul(fc1, weights['out']), biases['out'])
    return output

# tf Graph input
x = tf.placeholder(tf.float32, [None, 32, 32, 1])
y = tf.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32)

# Model
logits = conv_net(x, weights, biases, keep_prob)

# Define loss and optimizer
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
#softmax = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y)

# Accuracy
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()
        
with tf.Session() as sess:
    sess.run(init)
    data_index = 0
    
    for epoch in range(epochs):
        index = 0
        for batch in range(batch_size):
            
            batch_x, batch_y, index = nextBatch(training_data, training_labels, index)
            print ("batch_x : ", batch_x.shape, "index: " , index)
            sess.run(optimizer, feed_dict={x:  batch_x, y: batch_y, keep_prob: dropout})
#            print ("learning_rate: ", learning_rate)  

            # Calculate batch loss and accuracy
            loss = sess.run(cost, feed_dict={ x: batch_x, y: batch_y, keep_prob: .5})
            
            if previous_loss>0 and previous_loss - loss < 0:
                learning_rate = learning_rate*.95
                print ("decreasing learning_rate ",  learning_rate)

            previous_loss = loss
            
            valid_acc = sess.run(accuracy, feed_dict={ x: validation_data, y: validation_labels, keep_prob: 1.})
            training_acc = sess.run(accuracy, feed_dict={ x: training_data, y: training_labels, keep_prob: 1.})
            print(logits)

            print('Epoch {:>2} Loss: {:>10.4f} Validation Accuracy: {:.6f} training_acc {:.6f}'.format( epoch + 1, loss, valid_acc, training_acc))
            plot_data[data_index, 0] = loss
            plot_data[data_index, 1] = valid_acc
            plot_data[data_index, 2] = training_acc
            data_index = data_index + 1
            
#        learning_rate = learning_rate - .009
        
    # Calculate Test Accuracy
    test_acc = sess.run(accuracy, feed_dict={x: test_data, y: test_labels, keep_prob: 1.})
    confusion = tf.confusion_matrix(labels=test_labels, predictions=correct_pred, num_classes=n_classes)

    print('Testing Accuracy: {}'.format(test_acc))