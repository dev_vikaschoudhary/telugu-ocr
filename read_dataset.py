# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 15:15:22 2017

@author: vikas
"""

import matplotlib.pyplot as plt
from PIL import Image
import os
import numpy as np
from scipy.ndimage import rotate
import cv2

start = False
x_list = []
y_list = []      

penString = "pen_dataset1/"
batch_size = 90000
#2658
count =0
n_classes = 166
labels_list = []

training = np.empty(shape = (batch_size, 32, 32, 1), dtype=np.float)
labels = np.zeros(shape = (batch_size, n_classes), dtype=np.int)

def add_gaussian_noise(image_in, noise_sigma):
    temp_image = np.float64(np.copy(image_in))

    h = temp_image.shape[0]
    w = temp_image.shape[1]
    noise = np.random.randn(h, w) * noise_sigma

    noisy_image = np.zeros(temp_image.shape, np.float64)
    if len(temp_image.shape) == 2:
        noisy_image = temp_image + noise
    else:
        noisy_image[:,:,0] = temp_image[:,:,0] + noise
        noisy_image[:,:,1] = temp_image[:,:,1] + noise
        noisy_image[:,:,2] = temp_image[:,:,2] + noise

    """
    print('min,max = ', np.min(noisy_image), np.max(noisy_image))
    print('type = ', type(noisy_image[0][0][0]))
    """

    return noisy_image

def flip(img):
   width = img.shape[0]
   height = img.shape[1]
   for y in range(height):
       for x in range(width // 2):
           left = img[x, y]
           right = img[width - 1 - x, y]
           img[width - 1 - x, y] =  left
           img[x, y] = right

for directory in os.listdir(penString):
    for user_dir in os.listdir(penString + directory):
        filename = penString + directory + "/" + user_dir
        
        if ".jpg" in filename:
            continue

#        print("Reading " + filename)
        
        with open(filename, encoding="utf-8") as file:
            x = [l.strip() for l in file]
            label = ''
            for word in x:     
            
                if ".SEGMENT CHARACTER" in word:
                    z = word.split() 
                    label = z[4]
            
                if word==".PEN_UP":
                    start = False    
                
                if start:
                   z = word.split() 
                   x_list.append(int(z[0]))
                   y_list.append(int(z[1]))
                 
                if word==".PEN_DOWN":
                    start = True

            if count == batch_size:
                break

            if len(label)==0:
                continue

            label = label.replace('"', "")
            if int(label)<n_classes:

#                plt.plot(x_list, y_list, 'ro')
#                plt.axis('off')
#                    
#                img_file = penString + directory + "/" + label +'_.jpg'
#                        
#                if os.path.exists(img_file):
#                    img_file = penString + directory + "/" + label +'_1.jpg'
#                    
##                print (img_file)
#                plt.savefig(img_file)
#                jpgfile = Image.open(img_file)
#                jpgfile = jpgfile.resize((32, 32), Image.ANTIALIAS)
#                gray = jpgfile.convert('L')
#                
#    #            gray.save(img_file)
#                
#                arr = np.array(gray)
#                arr = arr.astype(float)
#                arr = arr*1/arr.max()
#                np.place(arr, arr==1, [0])
##                rotated_arr = rotate(arr, 5, reshape=False)
#                noisy_arr = add_gaussian_noise(arr, .01)
#                rotated_arr = rotate_bound(arr, 10)
                
#                rows,cols = arr.shape
#                M = cv2.getRotationMatrix2D((cols/2,rows/2),90,1)
#                rotated_arr = cv2.warpAffine(arr, M,(cols,rows))

#                pts1 = np.float32([[50,50],[200,50],[50,200]])
#                pts2 = np.float32([[10,100],[200,50],[100,250]])
#
#                M = np.float32([[1,0,10],[0,1,5]])
#                translated_arr = cv2.warpAffine(arr,M,(cols,rows))

#                arr[arr>0] = 1
    
    #            img = Image.fromarray(arr)
    #            img.save(img_file)
                
#                training[count,:, :, 0] = arr 
#                labels[count][int(label)] = 1
                labels_list.append(int(label))
                count = count +1
                
#                training[count,:, :, 0] = noisy_arr
#                labels[count][int(label)] = 1                
                labels_list.append(int(label))
                count = count +1
                
#                training[count,:, :, 0] = rotated_arr
#                labels[count][int(label)] = 1                
#                labels_list.append(int(label))
#                count = count +1
                
                plt.clf()
                
    #            jpgfile = Image.open(filename + '_TEMP_image.jpg')
    #            im = np.flipud(plt.imread(filename + '_TEMP_image.jpg'))
    #            plt.imshow(np.fliplr(im))
    #            plt.savefig( filename + '_real_image.jpg')
                
                start = False
                x_list = []
                y_list = [] 
                if count%100 == 0:
                    print ("count ", count)

#training.dump('data0_19_n_res32.pickle')
#labels.dump('lables0_19_n_res32.pickle')
np.asarray(labels_list).dump('lables_list0_19_n_res32.pickle')



