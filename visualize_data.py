# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 12:37:57 2018

@author: vikas
"""

import numpy as np
import matplotlib.pyplot as plt
import collections


#training = np.load('data0_166_res32_binary.pickle')
a = np.load('lables_list0_19_n_res32.pickle')

ctr = collections.Counter(a)
print("Frequency of the elements in the List : ",ctr)
labels, values = zip(*ctr.items())

indexes = np.arange(len(labels))
width = 1

plt.bar(indexes, values, width)
plt.xticks(indexes + width * 0.5, labels)
plt.show()
#plt.hist(labels[:], bins='auto')
#plt.show()